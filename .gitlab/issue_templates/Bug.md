<!--
Please read this!

Before opening a new issue, make sure to search for keywords in the issues
filtered by the "regression" or "bug" label.

The issue tracker can be found on Gitlab: https://gitlab.com/mattcan/the-stacks/issues

Please do your best to verify the issue you're about to submit isn't a duplicate.
-->

### Summary

<!-- Summarize the bug as concisely as possible -->

### Steps to reproduce

<!-- Add the steps required to reproduce the issue -->

### What is the current *bug* behaviour?

<!-- Describe the behaviour that is occurring -->

### What is the expected *correct* behavior?

<!-- Describe the behaviour that you expect to see -->

### Relevant logs, screenshots, and/or projects

<!--
Paste any relevant logs - please use code blocks (```) to format console output,
logs, and code as it's very hard to read otherwise.
-->

### Possible fixes

<!-- If you can, link to the line of code that might be responsible for the problem -->

/label ~bug
