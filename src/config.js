/*
The Stacks - A service for storing bookmarks
Copyright (C) 2018  Matthew Cantelon

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

const isDev = () => process.env.NODE_ENV === 'development'

const args = require('yargs-parser')(
  process.argv.slice(2),
  {
    alias: {
      host: ['h'],
      port: ['p']
    }
  }
)

const host = args.host || process.env.STACKS_HOST || 'http://localhost'
const port = args.port || process.env.STACKS_PORT || 8080

module.exports = {
  port,
  host,
  url: isDev() ? `${host}:${port}` : `${host}`,
  apiKeyName: 'X-STACKS-KEY',
  db: {
    url: isDev() ? 'http://localhost:5984' : ''
  }
}
