/*
The Stacks - A service for storing bookmarks
Copyright (C) 2018  Matthew Cantelon

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

const config = require('../../config')
const { NotFound } = require('../../errors')

const options = {
  schema: {
    description: 'Update an entry',
    summary: 'Update',

    security: [
      {
        ApiKeyAuth: []
      }
    ],

    body: {
      type: 'object',
      properties: {
        url: { type: 'string' },
        title: { type: 'string' },
        tags: {
          type: 'array',
          items: { type: 'string' }
        }
      },
      required: []
    },

    response: {
      201: {
        description: 'Successful response',
        type: 'object',

        properties: {
          id: { type: 'string' },
          url: { type: 'string' },
          title: { type: 'string' },
          tags: {
            type: 'array',
            items: { type: 'string' }
          }
        },

        headers: {
          type: 'object',
          properties: {
            Location: { type: 'string' }
          }
        }
      },

      404: {
        description: 'Entry not found',
        type: 'object',

        properties: {
          message: { type: 'string' },
          statusCode: { type: 'number' },
          error: { type: 'string' }
        }
      }

    }
  }
}

const handler = async (request, reply) => {
  const { entryClient } = request
  const { id } = request.params

  const foundEntry = await entryClient.retrieve(id)
  if (!foundEntry) {
    return new NotFound(`Unable to locate ${id}`)
  }

  const newEntry = Object.assign({ id }, request.body)
  entryClient.update(newEntry, foundEntry.version)

  reply.code(201).header('location', `${config.url}/entries/${id}`)
  return newEntry
}

module.exports = { options, handler }
