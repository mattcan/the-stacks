/*
The Stacks - A service for storing bookmarks
Copyright (C) 2018  Matthew Cantelon

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

// const config = require('../../config')
const { NotFound } = require('../../errors')

// const apiKeyName = config.apiKeyName.toLowerCase()

const options = {
  schema: {
    description: 'Retrieve an entry from the store',
    summary: 'Retrieve',

    security: [
      { ApiKeyAuth: [] }
    ],

    params: {
      type: 'object',
      properties: {
        id: { type: 'string' }
      }
    },

    response: {
      200: {
        description: 'Successful response',
        type: 'object',

        properties: {
          id: { type: 'string' },
          url: { type: 'string' },
          title: { type: 'string' },
          tags: {
            type: 'array',
            items: { type: 'string' }
          }
        }
      },

      404: {
        description: 'Unable to find entry',
        type: 'object',

        properties: {
          message: { type: 'string' },
          statusCode: { type: 'number' },
          error: { type: 'string' }
        }
      },

      401: {
        description: 'API Key is invalid',
        type: 'object',

        properties: {
          message: { type: 'string' },
          statusCode: { type: 'number' },
          error: { type: 'string' }
        }
      }
    }
  }
}

const handler = async (request, reply) => {
  const { entryClient } = request
  const { id } = request.params

  const foundEntry = await entryClient.retrieve(id)
  if (foundEntry) {
    reply.code(200)
    return foundEntry
  }

  return new NotFound(`Unable to locate ${id}`)
}

module.exports = { options, handler }
