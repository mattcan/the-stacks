/*
The Stacks - A service for storing bookmarks
Copyright (C) 2018  Matthew Cantelon

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

'use strict'

module.exports = async function (fastify, opts) {
  const retrieve = require('./retrieve')
  fastify.get('/entries/:id', retrieve.options, retrieve.handler)

  const update = require('./update')
  fastify.patch('/entries/:id', update.options, update.handler)

  const create = require('./create')
  fastify.post('/entries', create.options, create.handler)
}
