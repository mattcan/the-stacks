/*
The Stacks - A service for storing bookmarks
Copyright (C) 2018  Matthew Cantelon

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

const config = require('../../config')

const options = {
  schema: {
    description: 'Save an entry to the Stacks',
    summary: 'Save',

    security: [
      {
        ApiKeyAuth: []
      }
    ],

    body: {
      type: 'object',
      properties: {
        url: { type: 'string' },
        title: { type: 'string' },
        tags: {
          type: 'array',
          items: { type: 'string' }
        }
      }
    },

    response: {
      201: {
        description: 'Successful response',
        type: 'object',

        properties: {
          id: { type: 'string' },
          url: { type: 'string' },
          title: { type: 'string' },
          tags: {
            type: 'array',
            items: { type: 'string' }
          }
        },

        headers: {
          type: 'object',
          properties: {
            Location: { type: 'string' }
          }
        }
      },
      303: {
        description: 'The entry already exists',
        type: 'object',

        headers: {
          type: 'object',
          properties: {
            Location: { type: 'string' }
          }
        }
      }
    }
  }
}

const handler = async (request, reply) => {
  const { entryClient } = request

  const crypto = require('crypto')
  const id = crypto.createHash('md5').update(request.body.title).digest('hex')

  const foundEntry = await entryClient.retrieve(id)
  if (foundEntry) {
    request.log(`Found duplicate ${id}`)
    return reply.code(303).redirect(`${config.url}/entry/${id}`)
  }

  const newEntry = Object.assign({ id }, request.body)
  entryClient.create(newEntry)

  reply
    .code(201)
    .header('location', `${config.url}/entry/${id}`)

  return newEntry
}

module.exports = { options, handler }
