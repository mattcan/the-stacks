/*
The Stacks - A service for storing bookmarks
Copyright (C) 2018  Matthew Cantelon

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

module.exports = {
  routePrefix: '/documentation',
  exposeRoute: true,
  swagger: {
    info: {
      title: 'The Stacks',
      description: 'The Stacks, a self-hosted bookmarking service',
      version: '0.1.0'
    },
    externalDocs: {
      url: 'https://gitlab.com/mattcan/the-stacks#usage',
      description: 'Common usage scenarios'
    },
    host: 'localhost:8080', // TODO should pull from config
    schemes: ['http', 'https'],
    consumes: ['application/json', 'text/html'],
    produces: ['application/json', 'text/html'],
    securityDefinitions: {
      ApiKeyAuth: {
        type: 'apiKey',
        name: 'X-STACKS-KEY',
        in: 'header'
      }
    }
  }
}
