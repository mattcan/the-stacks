/*
The Stacks - A service for storing bookmarks
Copyright (C) 2018  Matthew Cantelon

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

'use strict'

const fp = require('fastify-plugin')
const { Unauthorized } = require('../errors/')

module.exports = fp(async function (fastify, options) {
  fastify.addHook('onRequest', async (request, reply) => {
    // Whitelis docs url
    const { url } = request.raw
    if (url.startsWith('/documentation')) { return }

    // Error if missing header
    const { Authorization } = request.headers
    if (!Authorization || !Authorization.startsWith('Bearer')) {
      throw new Unauthorized('Missing Authorization header with Bearer token')
    }

    // Verify header
  })
}, '2.x')
