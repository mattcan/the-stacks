/*
The Stacks - A service for storing bookmarks
Copyright (C) 2018  Matthew Cantelon

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

'use strict'

const fastify = require('fastify')(
  { logger: { level: 'debug' } }
)
const Autoload = require('fastify-autoload')
const path = require('path')
const config = require('./config')

fastify.register(require('./decorators/'))
fastify.register(Autoload, {
  dir: path.join(__dirname, 'features')
})

const start = async () => {
  try {
    await fastify.listen(config.port, config.host)
  } catch (err) {
    fastify.log.error('Killing server', err)
    process.exit(1)
  }
}

start()
