/*
The Stacks - A service for storing bookmarks
Copyright (C) 2018  Matthew Cantelon

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

const Entry = require('../models/Entry')

const DB_NAME = 'entries'

const removeId = (entry) => {
  const body = Object.assign({}, entry)
  delete body.id

  return body
}

class EntryClient {
  constructor (couchDbClient, logger) {
    this.db = couchDbClient.use(DB_NAME)
    this.logger = logger
  }

  /**
   * Get an entry out of storage
   *
   * @param {string} id - ID of the entry
   * @return {Entry|undefined}
   */
  async retrieve (id) {
    let entry = new Entry({})

    try {
      const dbEntry = await this.db.get(id)
      entry = new Entry(dbEntry)
    } catch (err) {
      console.log('Error getting entry', err)
      entry = undefined
    }

    return entry
  }

  /**
   * Create a new entry in storage
   *
   * @param {Entry} entry - The bookmark entry
   * @returns undefined
   * @throws {Error}
   */
  create (entry) {
    const { id } = entry
    const body = removeId(entry)

    this.db.insert(body, id, (err, result) => {
      if (err) { throw new Error('Unable to insert new document') }

      // TODO get a logger? Or return this?
      console.log(result)
    })
  }

  /**
   * Update an existing entry in storage
   *
   * @param {Entry} entry - The new details of the bookmark
   * @param {string} revision - The revision to update
   * @returns undefined
   * @throws {Error}
   */
  update (entry, revision) {
    const { id } = entry
    const body = removeId(entry)
    body._rev = revision

    this.db.insert(body, id, (err, result) => {
      if (err) { throw new Error('Unable to update document') }

      // TODO get a loggger or something
      console.log(result)
    })
  }
}

module.exports = EntryClient
