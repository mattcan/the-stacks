class NanoMock {
  use () {
    return this
  }

  insert () {
    return this
  }

  get () {
    return this
  }
}

module.exports = NanoMock
