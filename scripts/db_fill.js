/* eslint no-console: "off"  */
const faker = require('faker')
const nano = require('nano')({ url: 'http://localhost:5984' })

const { init } = require('./db_init')

const fakeEntry = () => ({
  title: faker.random.word(),
  url: faker.internet.url(),
  tags: faker.random.words().split(' ')
})

const filler = () => {
  // specify the database we are going to use
  const entries = nano.use('entries')

  for (let counter = 0; counter < 10; counter += 1) {
    const id = faker.random.uuid()
    // and insert a document in it
    entries.insert(fakeEntry(), id, (err, body) => {
      if (err) {
        console.log('[entries.insert] ', err.message)
        return
      }

      console.log('you have inserted the entry.')
      console.log(body)
    })
  }
}

init(filler)
