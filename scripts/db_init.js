const nano = require('nano')({ url: 'http://localhost:5984' })

module.exports = {
  init: (filler = null) => {
    // clean up the database we created previously
    nano.db.destroy('entries', () => {
      // create a new database
      nano.db.create('entries', () => {
        if (filler !== null) { filler() }
      })
    })
  }
}
