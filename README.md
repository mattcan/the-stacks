# 📚 The Stacks

> Named after the [library lingo for bookshelves][stacks-def], The Stacks is a
> self-hosted Micropub service.

[stacks-def]: https://www.trentu.ca/library/help/glossary/stacks

Are you someone who:

* prefers to own their own data?
* likes a challenge?
* is comfortable writing your own user interface?

If you answer yes to all of the questions above, The Stacks might be an ideal
bookmarking service for you.

### What is Micropub?

The [Micropub protocol](https://indieweb.org/Micropub) is a standard way for publishing based on
[Microformats](http://microformats.org/wiki/microformats2). The standard was
built so that owning content on your own site could be made simpler. If you own
a website but don't want to build out the publishing tools, running a Micropub
server and using any number of Micropub clients could be a path forward for you.

## Features

> I am an API

The Stacks is an API service, designed for easy interaction by developers or
other software.

> I support one user or multiple users

The Stacks has multi-tenant support so each user gets their own database for
bookmark storage. Want to help the most important people in your life start
taking ownership of their data? Hosting this service allows friend and family
to get their data out of the big silos.

## Usage

The Stacks is a service and its only interface is an API. To use it, it will
need to be deployed onto a server either in your home or on a cloud provider.

To run these queries, you need a running service. Please see the
[Deployment instructions](#deployment) to start your own service.

In the following examples consider `https://<api>/` to be your hosted endpoint.
You can copy, paste, and replace the `<api>` to run the examples on your own
machine.

### Save a bookmark

Likely the most common action to be taken.

**Note:** this is an optimistic call in that data is returned before a link is
actually saved

```sh
# Make a request
curl -X POST \
  https://<api>/entry \
  -H 'Content-Type: application/json' \
  -H 'X-STACKS-KEY: abc-123' \
  -d '{
  "title": "DuckDuckGo",
  "url": "https://duckduckgo.com/",
  "tags": [
    "search",
    "privacy",
    "ducks"
  ]
}'

# Receive a JSON response
{
    "id": "ab8415dd5798a360323ce06beaf30c35",
    "title": "DuckDuckGo",
    "url": "https://duckduckgo.com/",
    "tags": [
        "search",
        "privacy",
        "ducks"
    ]
}
```

### Get a bookmark

Have a bookmark ID? Get the details back using a `GET` request.

```sh
# Make a request
curl -X GET \
  https://<api>/entry/ab8415dd5798a360323ce06beaf30c35 \
  -H 'X-STACKS-KEY: abc-123'

# Receive a JSON response
{
    "id": "ab8415dd5798a360323ce06beaf30c35",
    "title": "DuckDuckGo",
    "url": "https://duckduckgo.com/",
    "tags": [
        "search",
        "privacy",
        "ducks"
    ]
}
```

### Archive a bookmark

Have a bookmark ID? Send a `PATCH` request with an `archive: true` in the body.

```sh
# Make a request
curl -X PATCH \
  https://<api>/entry/ab8415dd5798a360323ce06beaf30c35 \
  -H 'Content-Type: application/json' \
  -H 'X-STACKS-KEY: abc-123' \
  -d '{
  "archived": true
}'

# Receive a response
# 201?
```

### Delete a bookmark

Have a bookmark ID? Delete a bookmark using a `DELETE` request.

```sh
# Make a request
curl -X DELETE \
  https://<api>/entry/ab8415dd5798a360323ce06beaf30c35 \
  -H 'X-STACKS-KEY: abc-123'

# Receive a successful response
# 200?
```

### OpenAPI Docs

Visit http://hostname/documentation

When making requests, use `X-STACKS-KEY` with the value of `abc-123`.

## Roadmap

* key registration and validation
* multi-tenancy
* encrypted storage
* import and export

## Development

### Setup

1. If you have nvm: `nvm use`
1. `npm i`
1. `npm run db:start`
1. `npm run dev`

